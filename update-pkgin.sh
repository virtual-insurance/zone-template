cd /data

UPGRADE_TAR="bootstrap-trunk-x86_64-20201019-upgrade.tar.gz"

# Ensure you are running the latest package tools.
PKG_PATH=http://pkgsrc.joyent.com/packages/SmartOS/trunk/x86_64/All pkg_add -U pkg_install pkgin

# Unpack upgrade kit to /opt/local
tar -zxpf ${UPGRADE_TAR} -C /

# Upgrade all packages.
# pkgin -y upgrade
pkgin -y update
