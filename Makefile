

uuid = $(shell json uuid <vmadm.json || echo none)
iuuid = $(shell json image_uuid <vmadm.json || echo none)
image = /zones/$(iuuid)
zone = /zones/$(uuid)


# if the vmadm.json file has changed I will have to recreate the zone
# deleting a non existent zone should be ok
$(zone): vmadm.json $(image)
	-vmadm delete $(uuid)
	vmadm create -f vmadm.json
	./zlogin svcadm enable dns/multicast
	./zlogin sh /data/update-pkgin.sh

$(image):
	imgadm import $(iuuid)

.PHONY: reset
reset:
	if [ -f $(running) ]; then make stop; fi
	-vmadm delete $(uuid)
	-rm -rf data
	-rm Makefile vmadm.json

